fn main() {
    let a = vec![
        847, 847, 0, 0, 0, 399, 416, 416, 879, 879, 206, 206, 206, 272,
    ];
    println!("{:?}", apply_operations(a));
}

fn apply_operations(nums: Vec<i32>) -> Vec<i32> {
    let mut arr = nums;
    let mut count_zero = 0;

    for i in 0..arr.len() - 1 {
        if arr[i] == 0 {
            count_zero += 1;
        }

        if arr[i] == arr[i + 1] {
            arr[i] *= 2;
            arr[i + 1] = 0;
        }
    }

    let mut i = 0;
    while i < arr.len() && count_zero > 0 {
        if arr[i] == 0 {
            arr.remove(i);
            arr.push(0);
            count_zero -= 1;
        } else {
            i += 1;
        }
    }
    arr
}
