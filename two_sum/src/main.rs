use std::collections::HashMap;

fn main() {
    println!("Hello, world!");

    println!("{:?}", two_sum(vec![3,3], 6));
}

fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
    
    let mut map: HashMap<i32, i32> = HashMap::new();
    for (i,v) in nums.iter().enumerate() {
        let raz = target - v;
        match map.get(&raz) {
            Some(indx) => { return vec![*indx, i as i32]; },
            None => { map.insert(*v, i as i32 ); }
        }
    }
    return vec![];
}


