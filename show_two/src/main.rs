fn main() {
    println!("{}", test(5));
    println!("{}", test(-5));
}

fn test(a: i32) -> bool {
    if a <= 0 {
        for i in 0..a {
            println!("{i}");
        }
        return true;
    } else {
        return false;
    }
}

fn lambda() {
    let f = |a| { if a > 0 { for i in 0..=a { println!("{i}") } true} else { false } };
    println!("{:?}", f(5));
    println!("{:?}", f(-5));
}
