use std::vec;

fn main() {
    let mut a = vec![1,1,2,3,3,4];
    println!("{}", remove_duplicates(&mut a));
    println!("{:?}", a);
}

fn remove_duplicates(nums: &mut Vec<i32>) -> i32 {
    nums.dedup();
    nums.len() as i32
}
