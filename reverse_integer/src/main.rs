fn main() {
    println!("{}", reverse(-2147483648));
}

fn reverse(x: i32) -> i32 {
    let mut rez: i64 = 0;
    let mut my_x: i64 = x as i64;

    while my_x!=0 {
        rez = rez * 10 + my_x%10;
        my_x/=10;
        if rez > i32::MAX as i64 || rez < i32::MIN as i64 {
            return 0;
        }
    }

    if x<0 && rez>0 {
        rez*=-1
    }
    rez as i32
}
