
#[derive(Debug)]
enum RealCat {
    AliveCat { hungry: bool },
    DeadCat
}

fn main() {
    let realcat = RealCat::AliveCat { hungry: true };
    let realcat2 = RealCat::DeadCat;
    match &realcat {
        RealCat::AliveCat { hungry: false } => { println!("{:?}", realcat) },
        RealCat::AliveCat { hungry: true } => { println!("{:?}", realcat) },
        RealCat::DeadCat => { println!("{:?} dead", realcat) }
    }
    match &realcat2 {
        RealCat::AliveCat { .. } => { println!("{:?}", realcat) },
        RealCat::DeadCat => { println!("{:?} dead", realcat) }
    }
    
    let a = test(1, 3, "tes".to_string());
    println!()
}


fn test(num1: i32, num2: i32, str: String) -> i32 {
    println!("{}", str);
    return num1 + num2 * (num1 - num2)
}
