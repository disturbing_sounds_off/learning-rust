#[derive(Debug)]
struct Person {
    first_name: String,
    last_name: String,
    age: i32,
}

impl Person {

    fn new(first_name: &str, last_name: &str, age: i32) -> Person {
        return Person {
            first_name: first_name.to_string(),
            last_name: last_name.to_string(),
            age,
        }
    }

    fn print_self(self) {
        println!("firstname: {}, lastname: {}, age: {}", self.first_name, self.last_name, self.age);
    }

}

fn main() {
    let p = Person::new("Oleg", "Larionov", 32);
    p.print_self();
    test("".to_string())

}

fn test(text: String) {
    for i in text.into_bytes() {
        println!("{i}")
    }
}
