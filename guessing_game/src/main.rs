use std::io;

fn main() {
    println!("input number");
    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    let guess: i32 = guess.trim().parse().expect("Not a number");

    for i in 1..=guess {
        if guess%i ==0 {
            println!("{i}");
        }
    }
}
