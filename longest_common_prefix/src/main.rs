fn main() {
    println!("Hello, world!");
    let vector = vec!["flower".to_string(), "flow".to_string(), "flight".to_string()];
    println!("{}", longest_common_prefix(vector));
}

fn longest_common_prefix(strs: Vec<String>) -> String {
    let mut rez = "".to_string();
    let mut my_str = strs;

    my_str.sort();

    let first: Vec<char> = my_str[0].chars().collect();
    let last: Vec<char> = my_str[my_str.len()-1].chars().collect();

    for i in 0..first.len() {
        if first[i] != last[i] {
            break;
        } 
        rez.push(first[i]);
    }

    rez
}
