
fn main() {
    println!("Hello, world!");
    println!("{}", test("This is my stroka".to_string(), 15));
}

fn test(stroka: String, num: i32) -> i32 {
    
    let mut rez = num;
    for _ in stroka.chars() {
        rez+=1;
    }

    rez
}
