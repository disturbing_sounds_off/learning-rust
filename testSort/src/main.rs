fn main() {
    let mut a = vec![1, 6, 2, 50, -12, -5, 0, -1];

    for i in a.iter() {
        print!("{i}, ")
    }

    a.sort();
    println!("");

    for i in a.iter() {
        print!("{i}, ")
    }
}
