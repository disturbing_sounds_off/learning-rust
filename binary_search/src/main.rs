use rand::Rng;

fn print_array(arr: &[i32]){

    for (i, v) in arr.iter().enumerate() {
        print!("[{i}: {v}] ");
    }
    println!()
}


fn binary_search(num: i32, arr: &[i32]) -> i32 {

    
    let mut low = 0;
    let mut high = arr.len()-1;

    while low <= high {
        let mid = (high - low) / 2 + low; 
        let val = arr[mid];

        if val == num {
            return val;
        } 
        else if val < num {
            low = mid + 1;
        } 
        else {
            high = mid -1;    
        }
    }
    return -1
    
}

fn main() {

    let mut a: [i32; 100] = [-1;100];

    for i in 0..a.len() {
        a[i] = rand::thread_rng().gen_range(1..=100);
    }

    a.sort();
    print_array(&a);
    println!("-----------------------------------");
    
    let random_num = {

        let random_index = rand::thread_rng().gen_range( 1..={ a.len()-1 } );
        a[random_index]
    };
    let rez = binary_search(random_num, &a);

    println!("done");
    println!("The number was: {random_num}");
    if rez != -1 {
        println!("Found : {rez}");
    }
    else {
        println!("Number is not in array");
    }

}


