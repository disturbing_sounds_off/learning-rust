fn main() {
    println!("{}", is_palindrome(121));
    println!("{}", is_palindrome(-8));
    println!("{}", is_palindrome(0));

}

fn is_palindrome(x: i32) -> bool {
    if x < 0 {return false;}

    let mut rev = 0;
    let mut temp = x;

    while temp != 0 {
        rev = rev * 10 + temp % 10;
        temp/=10;
    }

    x == rev
}

